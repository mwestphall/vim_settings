# Vim_Settings
My `~/.vim` directory  
`git clone` into `~/.vim`  
run `echo runtime vimrc > ~/.vimrc` to have vim use this repo's `vimrc`  
Uses the monokai colorscheme by sickill (https://github.com/sickill/vim-monokai)